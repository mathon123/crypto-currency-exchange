import { IsString, IsIn, IsBooleanString } from 'class-validator';
import { supportedCurrencies, supportedCryptoCurrencies } from '../utils/settings';

class GetCurrencyRateDto {
  @IsString()
  @IsIn(supportedCryptoCurrencies.map(crypto => crypto.toUpperCase()))
  public cryptoCurrency: string;

  @IsString()
  @IsIn(supportedCurrencies)
  public fiatCurrency: string;

  @IsBooleanString()
  public reSync?: boolean;
}

export default GetCurrencyRateDto;

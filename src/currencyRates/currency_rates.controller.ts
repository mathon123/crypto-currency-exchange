import { Request, Response, NextFunction, Router } from 'express';
import Controller from '../interfaces/controller.interface';
import validationMiddleware from '../middleware/validation.middleware';
import GetCurrencyRateDto from './currency_rates.dto';
import CryptoCurrencyExchangeService from '../services/CryptoCurrencyExchangeService';
import { ExternalApiType } from '../commonTypes/externalApiServiceType.type';
class CurrencyRatesController implements Controller {
  public path = '/rates';
  public router = Router();

  constructor(private readonly externalApiService: ExternalApiType) {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(this.path, validationMiddleware(GetCurrencyRateDto), this.getCurrencyRate);
  }

  private getCurrencyRate = async (req: Request, res: Response) => {
    const exchangeService = new CryptoCurrencyExchangeService(this.externalApiService);
    const { cryptoCurrency, fiatCurrency, reSync } = req.query;

    if (reSync === 'true') {
      await this.externalApiService.sync();
    }
    return res.status(200).send({
      exchangeRate: exchangeService.calculateRate(cryptoCurrency, fiatCurrency),
      cryptoCurrencyCode: cryptoCurrency,
      fiatCurrencyCode: fiatCurrency,
      timestamp: this.externalApiService.getTimestamp(),
    });
  }
}

export default CurrencyRatesController;

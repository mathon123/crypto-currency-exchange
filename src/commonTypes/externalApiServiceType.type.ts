import ExternalApiService from '../services/ExternalApiService';

export type ExternalApiType = typeof ExternalApiService;

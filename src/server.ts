import 'dotenv/config';
import App from './app';
import validateEnv from './utils/validateEnv';
import CurrencyRatesController from './currencyRates/currency_rates.controller';
import externalApiService from './services/ExternalApiService';
import { cronTask } from './services/CronService';

validateEnv();

const app = new App(
  [
    new CurrencyRatesController(externalApiService),
  ],
);

externalApiService.sync()
    .then(() => {
      cronTask(externalApiService);
      app.listen();
    })
    .catch((e) => {
      console.log(e);
      process.exit(1);
    });

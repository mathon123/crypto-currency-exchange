import { mainCurrency } from '../utils/settings';
import { ExternalApiType } from '../commonTypes/externalApiServiceType.type';

class CryptoCurrencyExchangeService {
  constructor(private externalApiService: ExternalApiType) {}

  public calculateRate(cryptoCurrency: string, fiatCurrency: string) {
    const currencyRates = this.externalApiService.getCurrencyRates();
    const cryptoCurrencyRates = this.externalApiService.getCryptoCurrencyRates();

    const byName = ({ name }: { name: string }) => name === cryptoCurrency.toLowerCase();
    const { value: mainCurrencyCryptoRate } = cryptoCurrencyRates.find(byName);

    const currentExchangeRate = currencyRates[`${mainCurrency}${fiatCurrency}`];

    return mainCurrencyCryptoRate * currentExchangeRate;
  }
}

export default CryptoCurrencyExchangeService;

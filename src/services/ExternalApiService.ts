import axios, { AxiosInstance } from 'axios';
import { supportedCryptoCurrencies, mainCurrency } from '../utils/settings';

type CryptoCurrencyDetailsType = {
  id: string;
  symbol: string;
  name: string;
};

class ExternalApiService {
  private apiClient: AxiosInstance;
  private currencyRatesData: any;
  private cryptoRatesData: any;
  private timestamp: number;

  private cryptoCurrenciesDetails: CryptoCurrencyDetailsType[] = [];

  constructor() {
    this.apiClient = axios.create();
  }

  public getCurrencyRates() {
    return this.currencyRatesData;
  }

  public getCryptoCurrencyRates() {
    return this.cryptoRatesData;
  }

  public getTimestamp() {
    return this.timestamp;
  }

  public async setCryptoRates() {
    if (this.cryptoCurrenciesDetails.length === 0) {
      this.cryptoCurrenciesDetails = await this.getSupportedCryptoCurrenciesDetails();
    }
    const { data: externalCryptoData } = await this.getExternalCryptoRates();

    this.cryptoRatesData = this.formatExternalCryptoData(externalCryptoData);
  }

  public async sync() {
    return Promise.all([
      this.setCurrencyRates(),
      this.setCryptoRates(),
    ]).then(() => this.timestamp = new Date().getTime());
  }

  public async setCurrencyRates() {
    this.currencyRatesData = await this.getExternalCurrencyRates();
  }

  private async getExternalCryptoRates() {
    const cryptoCurrenciesIds = this.cryptoCurrenciesDetails.map(detail => detail.id);

    return this.apiClient.get(`${process.env.COIN_GECKO_API}/simple/price`, {
      params: {
        ids: cryptoCurrenciesIds.join(','),
        vs_currencies: mainCurrency,
      },
    });
  }

  private async getExternalCurrencyRates() {
    const response = await this.apiClient.get(process.env.CURRENCY_LAYER_API, {
      params: {
        source: mainCurrency,
      },
    });
    return response.data.quotes;
  }

  private async getSupportedCryptoCurrenciesDetails() {
    const response = await this.apiClient.get(`${process.env.COIN_GECKO_API}/coins/list`);
    const data = response.data;

    const isSupported = (coinDetails: CryptoCurrencyDetailsType) => supportedCryptoCurrencies.includes(coinDetails.symbol);
    return data.filter(isSupported);
  }

  private formatExternalCryptoData(externalCryptoData: any) {
    return Object.keys(externalCryptoData).map((cryptoRateName: string) => {
      const { symbol } = this.cryptoCurrenciesDetails.find(details => details.id === cryptoRateName);
      return { name: symbol, value: externalCryptoData[cryptoRateName][mainCurrency.toLowerCase()] };
    });
  }
}

export default new ExternalApiService();

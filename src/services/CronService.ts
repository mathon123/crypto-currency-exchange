import { schedule } from 'node-cron';
import { ExternalApiType } from '../commonTypes/externalApiServiceType.type';

export const cronTask = (externalApiService: ExternalApiType) => {
  return schedule('* * * * *', () =>  externalApiService.sync(), {
    scheduled: true,
  });
};

import {
    cleanEnv, port, str,
} from 'envalid';

function validateEnv() {
  cleanEnv(process.env, {
    PORT: port(),
      CURRENCY_LAYER_API: str(),
  });
}

export default validateEnv;

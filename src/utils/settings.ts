export const supportedCryptoCurrencies = [
  'usdt',
  'btc',
  'eth',
  'xrp',
  'ltc',
  'eos',
  'bch',
  'trx',
  'link',
  'ada',
  'dot',
  'bnb',
  'bsv',
  'usdc',
  'xmr',
  'bch',
];

export const supportedCurrencies = [
  'USD',
  'EUR',
  'BGN',
  'AMD',
  'AFN',
];

export const mainCurrency = 'USD';
